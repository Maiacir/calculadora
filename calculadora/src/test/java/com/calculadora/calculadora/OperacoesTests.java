package com.calculadora.calculadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


import java.util.ArrayList;
import java.util.List;

public class OperacoesTests {
    @Test
    public void testarOperacaoDeSoma() {
        List<Integer> numeros = new ArrayList<>();
        numeros.add(1);
        numeros.add(2);
        numeros.add(3);
        Assertions.assertEquals(OPeracao.soma(numeros), 6);

    }

    public void testarOperacaoDeDoisNumeros() {
        Assertions.assertEquals(OPeracao.soma(1,1), 2);
    }

    public void testarOPeracaoDeSubtracao() {
        List<Integer> numeros = new ArrayList<>();
        numeros.add(10);
        numeros.add(5);
        numeros.add(1);
        Assertions.assertEquals(OPeracao.listasubtracao(numeros),4);
    }

    public void testarOperacaoDeDoisNumSub() {
        Assertions.assertEquals(OPeracao.subtracao(5, 1), 4);
    }

    public void testarOperacaoDeDivisao() {
        Assertions.assertEquals(OPeracao.divisao(20,2), 10);
    }

    public void testarOperacaoDeMultiplicacao () {
        List<Integer> numeros = new ArrayList<>();
        numeros.add(2);
        numeros.add(2);
        numeros.add(3);
        Assertions.assertEquals(OPeracao.listamultiplicacao(numeros), 12);
    }

    public void testarOperacaoDeDoisNumMultiplicacao () {
        Assertions.assertEquals(OPeracao.multiplicacao(2,2), 4);
    }


}


