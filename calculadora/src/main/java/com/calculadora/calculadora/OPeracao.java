package com.calculadora.calculadora;

import org.omg.CORBA.INTERNAL;
import org.omg.PortableInterceptor.INACTIVE;

import java.util.List;

public class OPeracao {

    public static int soma(int numUm, int numDois) {
        int resultado = numUm + numDois;
        return resultado;
    }

    public static int soma(List<Integer> numeros) {
        int resultado = 0;
        for (Integer numero:numeros) {
            resultado += numero;
        }
        return resultado;
    }

    public static int listasubtracao(List<Integer> numeros) {
        int resultado = 0;
        for (Integer numero:numeros) {
            resultado -= numero;
        }
        return resultado;
    }

    public static int subtracao(int numUm, int numDois) {
        int resultado = numUm - numDois;
        return resultado;
    }

    public static double divisao(int numUm, int numDois) {
        double resultado = numUm / numDois;
        return resultado;

    }

    public static int listamultiplicacao(List<Integer> numeros) {
        int resultado = 0;
        for (Integer numero:numeros) {
            resultado *= numero;
        }
        return resultado;
    }
    public static int multiplicacao(int numUm, int numDois) {
        int resultado = numUm * numDois;
        return resultado;
    }

}
